﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ToDoApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Task> tasksList = new ObservableCollection<Task>();

        public MainWindow()
        {
            InitializeComponent();

            ListBox.ItemsSource = tasksList;
            ListBox.DisplayMemberPath = "Name";
        }

        private void ListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Task selected = ListBox.SelectedItem as Task;
            if (selected != null)
            {
                MessageBox.Show(selected.Description);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            NewTaskWindow window = new NewTaskWindow();
            window.Owner = this;    //Як центрувати дане вікно
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if (window.ShowDialog() == true)
            {
                Task newTask = window.Result;
                tasksList.Add(newTask);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            int index = ListBox.SelectedIndex;
            if (index != -1) { tasksList.RemoveAt(index); }
        }

        private void CompleteButton_Click(object sender, RoutedEventArgs e)
        {
            int index = ListBox.SelectedIndex;
            if (index != -1) { tasksList[index].IsCompleted = true; }
        }

        private void AllRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            ListBox.ItemsSource = tasksList;
        }

        private void NotCompletedRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            Filter("NotCompleted");
        }

        private void CompletedRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            Filter("Completed");
        }

        private void Filter(string type)
        {
            ObservableCollection<Task> filtered = new ObservableCollection<Task>();
            for (int i = 0; i < tasksList.Count; i++)
            {
                Task current = tasksList[i];
                if (type == "NotCompleted" && !current.IsCompleted) { filtered.Add(current); }
                if (type == "Completed" && current.IsCompleted) { filtered.Add(current); }
            }

            ListBox.ItemsSource = filtered;
        }

        string fileName = "data.bin";

        private void Window_Closed(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Stream file = File.OpenWrite(fileName);
            formatter.Serialize(file, tasksList);
            file.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (File.Exists(fileName))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                Stream file = File.OpenRead(fileName);
                tasksList = formatter.Deserialize(file) as ObservableCollection<Task>;
                file.Close();
                ListBox.ItemsSource = tasksList;
            }
        }
    }
}
